import {Navigation} from 'react-native-navigation';
import {SIDE_MENU_ID} from '../components';

export const toogleDrawer = props => {
  Navigation.mergeOptions(SIDE_MENU_ID, {
    sideMenu: {
      left: {
        visible: true,
      },
    },
  });
};
