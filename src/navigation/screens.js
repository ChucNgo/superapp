import {TAB1_SCREEN, TAB2_SCREEN} from '../screens';
import views from './views';
import {SIDE_MENU_ID} from '../components';
import colors from '../resources/colors';
import {Navigation} from 'react-native-navigation';

const tab1Screen = icon => ({
  stack: {
    children: [
      {
        component: {
          name: TAB1_SCREEN,
          options: {
            topBar: {
              visible: true,
              title: {
                text: 'TopBar',
              },
              backButton: {
                title: 'Home',
                showTitle: true,
                visible: true,
              },
              background: {},
            },
          },
        },
      },
    ],
    options: {
      bottomTab: {
        text: 'Chat',
        icon,
      },
    },
  },
});

const tab2Screen = icon => ({
  stack: {
    children: [
      {
        component: {
          name: TAB2_SCREEN,
          options: {
            topBar: {
              visible: true,
              title: {
                text: 'TopBar',
              },
              backButton: {
                title: 'Home',
                showTitle: true,
                visible: true,
              },
            },
          },
        },
      },
    ],
    options: {
      bottomTab: {
        text: 'Friend',
        icon,
      },
    },
  },
});

const bottomTabs = sourcesIcon => ({
  root: {
    bottomTabs: {
      children: [tab1Screen(sourcesIcon[0]), tab2Screen(sourcesIcon[1])],
      options: {
        backgroundColor: colors.white,
      },
    },
  },
});

const main = () => ({
  root: {
    sideMenu: {
      id: SIDE_MENU_ID,
      left: views.sideMenuLeft(),
      center: {
        stack: {
          children: [views.map(), views.home()],
        },
      },
      openGestureMode: 'entireScreen' | 'bezel',
      animationType: 'slide', //'parallax', 'door', 'slide', or 'slide-and-scale'
      options: {
        sideMenu: {
          left: {
            width: 260,
          },
        },
      },
    },
  },
});

export default {
  tab1Screen,
  tab2Screen,
  bottomTabs,
  main,
};
