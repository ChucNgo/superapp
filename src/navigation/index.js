import {Navigation} from 'react-native-navigation';
import {gestureHandlerRootHOC, FlatList} from 'react-native-gesture-handler';
import {withReduxProvider} from '../store';
import {
  Home,
  HOME_SCREEN,
  MAP_SCREEN,
  MapView,
  TAB1_SCREEN,
  TAB2_SCREEN,
  Tab1,
  Tab2,
} from '../screens';
import components, {
  SIDE_MENU_COMPONENT,
  ICON_MENU_COMPONENT,
} from '../components';
import screens from './screens';
import views from './views';

import Entypo from 'react-native-vector-icons/Entypo';
import colors from '../resources/colors';
import {Platform} from 'react-native';

const ios = Platform.OS === 'ios' ? true : false;

const Screens = new Map();

Screens.set(HOME_SCREEN, Home);
Screens.set(MAP_SCREEN, MapView);
Screens.set(SIDE_MENU_COMPONENT, components.SideMenu);
Screens.set(TAB1_SCREEN, Tab1);
Screens.set(TAB2_SCREEN, Tab2);
Screens.set(ICON_MENU_COMPONENT, components.IconMenu);

Screens.forEach((C, key) => {
  Navigation.registerComponent(
    key,
    () => gestureHandlerRootHOC(withReduxProvider(C)),
    () => C,
  );
});

export const startApp = () => {
  Navigation.setRoot(screens.main());
};

export const pushBottomTabs = () => {
  Navigation.setDefaultOptions({
    bottomTab: {
      textColor: colors.gray,
      selectedTextColor: colors.black,
      iconColor: colors.gray,
      selectedIconColor: colors.black,
      fontSize: 12,
      fontWeight: 'bold',
    },
  });

  Promise.all([
    Entypo.getImageSource('chat', 22),
    Entypo.getImageSource('users', 22),
  ]).then(sources => {
    Navigation.setRoot(screens.bottomTabs(sources));
  });
};

export default {
  screens,
  views,
};
