import {HOME_SCREEN, MAP_SCREEN} from '../screens';
import {
  ICON_MENU_COMPONENT,
  ICON_MENU_ID,
  SIDE_MENU_COMPONENT,
} from '../components';
import {Platform} from 'react-native';

const ios = Platform.OS === 'ios' ? true : false;

const sideMenuLeft = () => ({
  component: {
    name: SIDE_MENU_COMPONENT,
  },
  shouldStretchDrawer: false,
});

const map = () => ({
  component: {
    name: MAP_SCREEN,
    options: {
      topBar: {
        visible: true,
      },
    },
  },
});

const home = () => {
  return {
    component: {
      name: HOME_SCREEN,
      options: {
        topBar: {
          visible: true,
          backButton: {
            visible: false,
          },
          title: {
            text: 'Home',
          },
          statusBar: {
            style: 'light',
          },
          leftButtons: [
            {
              id: ICON_MENU_ID,
              component: {
                name: ICON_MENU_COMPONENT,
              },
              icon: ios
                ? {}
                : {
                    uri: 'ic_menu',
                  },
            },
          ],
        },
      },
    },
  };
};

export default {
  home,
  map,
  sideMenuLeft,
};
