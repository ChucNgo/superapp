import React, {Component} from 'react';
import {View} from 'react-native';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import OneSignal from 'react-native-onesignal';

class FirebaseNoti extends Component {
  constructor(props) {
    super(props);
    OneSignal.init('a2b93bc4-3366-4a62-8f39-d7f58a61a40c', {
      kOSSettingsKeyAutoPrompt: true,
    });
  }

  async componentDidMount() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
    } else {
      try {
        await firebase.messaging().requestPermission();
        // User has authorised
        let token = await firebase.messaging().getToken();
        console.log(token);
      } catch (error) {
        // User has rejected permissions
        console.log('permission rejected');
      }
    }

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const action = notificationOpen.action;
      const notification = notificationOpen.notification;
      let seen = [];
    }

    const channel = new firebase.notifications.Android.Channel(
      'test-channel',
      'Test Channel',
      firebase.notifications.Android.Importance.Max,
    ).setDescription('My App test channel');
    // Create channel
    firebase.notifications().android.createChannel(channel);
    this.notificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed(noti => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID.
        // You will have to specify this manually if you'd like to
        // re-display the notification.
      });

    this.notificationListener = firebase
      .notifications()
      .onNotification(noti => {
        console.log(noti);
        // Process your notification as required
        noti.android
          .setChannelId('test-channel')
          .android.setSmallIcon('ic_launcher')
          .android.setBigText(noti.body);
        firebase.notifications().displayNotification(noti);
      });

    // Gọi khi ấn vào noti
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notiOpen => {
        // Get the action triggered by the noti opened
        const action = notiOpen.action;

        // Get infomation about the noti that was opened
        const noti = notiOpen.notification;

        let seen = [];

        firebase
          .notifications()
          .removeDeliveredNotification(noti.notificationId);
      });
  }

  componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  render() {
    return <View></View>;
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FirebaseNoti);
