import Home from './Home';
import MapView from './Map';
import Tab1 from './Tab1';
import Tab2 from './Tab2';

export {Home, MapView, Tab1, Tab2};

export const HOME_SCREEN = 'superapp.HomeScreen';
export const MAP_SCREEN = 'superapp.MapScreen';
export const TAB1_SCREEN = 'superapp.Tab1Screen';
export const TAB2_SCREEN = 'superapp.Tab2Screen';
export const TAB3_SCREEN = 'superapp.Tab3Screen';
