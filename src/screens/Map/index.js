import MapScreen from './MapScreen';
import {connect} from 'react-redux';

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MapScreen);
