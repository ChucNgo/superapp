import HomeScreen from './HomeScreen';
import {connect} from 'react-redux';
import {showOrHideDrawer} from '../../modules/root/actions';
import {bindActionCreators} from 'redux';

const mapStateToProps = state => ({
  isVisibleDrawer: state.root.isVisibleDrawer,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({showOrHideDrawer}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
