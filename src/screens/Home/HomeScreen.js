import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import colors from '../../resources/colors';
import {Navigation} from 'react-native-navigation';
import {useNavigationButtonPress} from 'react-native-navigation-hooks';
import nav, {pushBottomTabs} from '../../navigation';
import {toogleDrawer} from '../../navigation/common';
import {ICON_MENU_ID} from '../../components';
import FirebaseNoti from '../../utils/FirebaseNoti';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
let Analytics = firebase.analytics();
const HomeScreen = props => {
  const [isLogin, setLoginStatus] = useState(false);

  useNavigationButtonPress(
    e => {
      // props.showOrHideDrawer();
      toogleDrawer(props);
    },
    props.componentId,
    ICON_MENU_ID,
  );

  const navigate = screenName => {
    Navigation.push(props.componentId, screenName);
  };

  const loginFacebook = () => {
    if (!isLogin) {
      LoginManager.logInWithPermissions([]).then(
        result => {
          if (result.isCancelled) {
            alert('Login Canceled');
            console.log('login is cancelled.');
          } else {
            setLoginStatus(true);
            alert('Login Success');
            console.log(
              'Login success with permissions: ' +
                result.grantedPermissions.toString(),
            );
          }
        },
        error => {
          alert('Login Error');
          console.log('login has error: ' + error);
        },
      );
    } else {
      LoginManager.logOut();
      setLoginStatus(false);
      console.log('logout.');
    }
  };

  const logAnalytics = () => {
    Analytics.logEvent('test', {key: '0'});
  };

  const logCrashlytics = () => {
    firebase.crashlytics().crash();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigate(nav.views.map())}>
        <Text style={styles.text}>Go To Maps</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={logAnalytics}>
        <Text style={styles.text}>Log Analytics</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={logCrashlytics}>
        <Text style={styles.text}>Force Crash</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={loginFacebook}>
        <Text style={styles.text}>Log {isLogin ? 'out' : 'in'} Facebook</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => pushBottomTabs()}>
        <Text style={styles.text}>Go To Tabbar</Text>
      </TouchableOpacity>

      <FirebaseNoti />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.green_dark,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.green_dark,
    fontSize: 18,
    fontWeight: 'bold',
  },
  button: {
    width: 180,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.aqua,
    paddingVertical: 10,
    borderRadius: 5,
    marginBottom: 24,
  },
});

export default HomeScreen;
