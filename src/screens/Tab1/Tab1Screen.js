import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import colors from '../../resources/colors';
import {useNavigationButtonPress} from 'react-native-navigation-hooks';
import {startApp} from '../../navigation';

export default props => {
  useNavigationButtonPress(e => {
    startApp();
  }, props.componentId);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Chat Screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.green_dark,
    fontSize: 18,
    fontWeight: 'bold',
  },
});
