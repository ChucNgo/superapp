export default {
  white: '#FFFFFF',
  black: '#000000',
  green_dark: '#121E21',
  aqua: 'aqua',
  gray: '#D0D0D0',
  red: 'red',
};
