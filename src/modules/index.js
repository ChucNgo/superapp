import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import root from './root';

const allReducers = combineReducers({
  form: formReducer,
  root,
});

export default allReducers;
