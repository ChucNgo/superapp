import types from './types';

export const showOrHideDrawer = () => ({
  type: types.SHOW_OR_HIDE_DRAWER,
});
