import reducer from './reducers';
import types from './types';
import actions from './actions';

export {types, actions};

export default reducer;
