import types from './types';

const initialState = {
  isVisibleDrawer: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SHOW_OR_HIDE_DRAWER:
      return {...state, isVisibleDrawer: !state.isVisibleDrawer};
    default:
      return state;
  }
};
