import React from 'react';
import {TouchableOpacity} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import colors from '../../resources/colors';
import {toogleDrawer} from '../../navigation/common';
import {connect} from 'react-redux';
import {showOrHideDrawer} from '../../modules/root/actions';
import {bindActionCreators} from 'redux';

const IconMenu = props => {
  const onPress = () => {
    props.showOrHideDrawer();
    toogleDrawer(props);
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <Feather name="menu" size={20} color={colors.black} />
    </TouchableOpacity>
  );
};

const mapStateToProps = state => ({
  isVisibleDrawer: state.root.isVisibleDrawer,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({showOrHideDrawer}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(IconMenu);
