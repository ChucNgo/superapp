import SideMenu from './SideMenu';
import IconMenu from './IconMenu';

export const SIDE_MENU_COMPONENT = 'superapp.SideMenu';
export const ICON_MENU_COMPONENT = 'superapp.IconMenu';

export const SIDE_MENU_ID = 'superapp.SideMenuId';
export const ICON_MENU_ID = 'superapp.IconMenuId';

export default {
  SideMenu,
  IconMenu,
};
