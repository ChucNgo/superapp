/**
 * @format
 */

import {Navigation} from 'react-native-navigation';
import {startApp, pushBottomTabs} from './src/navigation';
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
Navigation.events().registerAppLaunchedListener(
  () => startApp(),
  // pushBottomTabs(),
);
