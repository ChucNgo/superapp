//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Hakintosher on 2/7/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
